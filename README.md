# Using GitLab as Private Maven Repository

_tbd_



## References

* [Git as a secure private Maven repository](https://jeroenmols.com/blog/2016/02/05/wagongit/)

* [Maven private repository in gitlab](https://ubs-soft.com/maven-private-repository-in-gitlab/)

* [ Introduction to GitLab CI with Maven](https://labs.consol.de/development/2017/07/14/gitlab-ci.html)



